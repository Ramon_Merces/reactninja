'use strict'

import React, { PropTypes } from 'react'
import Search from '../searchbar/search'
import UserInfo from '../userinfo/userinfo'
import Actions from '../actions/actions'
import Repos from '../repos/repos'

const AppContent = ({ userinfo, repos, favo, handleSearch, handleActionRepos, handleActionfavs }) => (
  <div className='app'>
    <Search handleSearch={handleSearch} />
    {!!userinfo && <UserInfo userinfo={userinfo} />}
    {!!userinfo && <Actions getRepos={handleActionRepos} getFavs={handleActionfavs} />}
    {!!repos.length && <Repos
      className='repos'
      title='Repositórios'
      repos={repos} />}
    {!!favo.length && <Repos
      className='stared'
      title='Favoritos'
      repos={favo} />}
  </div>
)
AppContent.propTypes = {
  userinfo: PropTypes.object,
  repos: PropTypes.array.isRequired,
  favo: PropTypes.array.isRequired
}
export default AppContent

'use strict'

import React from 'react'

const Actions = ({ getRepos, getFavs }) => (
  <div className='actions'>
    <button onClick={getRepos}>Ver Repositórios</button>
    <button onClick={getFavs}>Ver Favoritos</button>
  </div>
)

export default Actions

'use strict'

import React from 'react'

const Search = ({ handleSearch }) => (
  <div className='search'>
    <input placeholder='Procurar por usuário'
      onKeyUp={handleSearch} />
  </div>
)
Search.propTypes = {
  handleSearch: React.PropTypes.func.isRequired
}
export default Search

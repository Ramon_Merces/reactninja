'use strict'

import React from 'react'
import Button from './button'

const SearchButton = () => (
  <Button handleClick={() => alert('Search')}>
    Search Button
  </Button>
)

export default SearchButton

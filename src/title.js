'use strict'

import React from 'react'

const Title = ({ name, lastname }) => (
  <h1>{`${name} ${lastname}`} </h1>
)

Title.defaultProps = {
  name: 'Desconhecido',
  lastname: 'sem sobrenome'
}
/*
React.createClass({
  getDefaultProps: function () {
    return {
      name: 'Desconhecido',
      lastname: 'sem sobre nome'
    }
  },
  render: function () {
    return (
      <h1>{this.props.name + ' ' + this.props.lastname } </h1>
    )
  }
})
*/
export default Title

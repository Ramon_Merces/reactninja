'use strict'

import React from 'react'
import { render } from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import App from './app'

const renderAPP = (NextAPP) => {
  render(
    <AppContainer>
      <NextAPP />
    </AppContainer>,
    document.querySelector('[data-js="app"]')
  )
}
renderAPP(App)
if (module.hot) {
  module.hot.accept('./app', () => {
    const NextApp = require('./app').default
    renderAPP(NextApp)
  })
}

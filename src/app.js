'use strict'

import React, { Component } from 'react'
import AppContent from './components/app-content/app-content'
import ajax from '@fdaciuk/ajax'

class App extends Component {
  constructor () {
    super()
    this.state = {
      userinfo: null,
      repos: [],
      favo: []
    }
  }

  handleSearch (e) {
    const value = e.target.value
    const KeyCode = e.which || e.keyCode
    const ENTER = 13
    if (KeyCode === ENTER) {
      ajax().get(`https://api.github.com/users/${value}`)
        .then((result) => {
          this.setState({
            userinfo: {
              username: result.name,
              photo: result.avatar_url,
              login: result.login,
              repos: result.public_repos,
              followers: result.followers,
              following: result.following
            }
          })
        }
        )
    }
  }
  handleActionRepos () {
    ajax().get(`https://api.github.com/users/${this.state.userinfo.login}/repos`)
      .then((result) => {
        this.setState({
          repos: result
        })
      })
  }
  handleActionfavs () {
    ajax().get(`https://api.github.com/users/${this.state.userinfo.login}/starred`)
      .then((result) => {
        console.log(result)
      })
  }
  render () {
    return (
      <AppContent
        userinfo={this.state.userinfo}
        repos={this.state.repos}
        favo={this.state.favo}
        handleSearch={(e) => this.handleSearch(e)}
        handleActionRepos={() => this.handleActionRepos()}
        handleActionfavs={() => this.handleActionfavs()} />
    )
  }
}
export default App
